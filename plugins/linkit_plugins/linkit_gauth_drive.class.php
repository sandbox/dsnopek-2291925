<?php
/**
 * @file
 * Contains LinkitGAuthDrivePlugin.
 */

class LinkitGAuthDrivePlugin extends LinkitPlugin {
  /**
   * Query Google for the results.
   */
  function queryGoogle() {
    global $user;

    if ($client_path = libraries_get_path('google-api-php-client')) {
      if ($client = linkit_gauth_get_client($user, 'drive')) {
        require_once($client_path . '/src/contrib/Google_DriveService.php');
        $service = new Google_DriveService($client);

        try {
          $search_string = str_replace("'", "\\'", $this->search_string);
          $result = $service->files->listFiles(array(
            'q' => "title contains '$search_string'",
            'maxResults' => 10,
          ));
          return $result['items'];
        }
        catch(Google_ServiceException $e) {
          watchdog('linkit_gauth', 'Unable to query Google Drive: %message', array('%message' => $e->getMessage()), WATCHDOG_ERROR);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  function buildLabel($item) {
    $label = $item['title'];
    return check_plain($label);
  }

  /**
   * Build the search row description.
   */
  function buildDescription($item) {
    switch ($item['mimeType']) {
      case 'application/vnd.google-apps.audio':
        return 'Audio';

      case 'application/vnd.google-apps.document':
        return 'Google Document';

      case 'application/vnd.google-apps.drawing':
        return 'Google Drawing';

      case 'application/vnd.google-apps.file':
        return 'File';

      case 'application/vnd.google-apps.folder':
        return 'Folder';

      case 'application/vnd.google-apps.forms':
        return 'Google Form';

      case 'application/vnd.google-apps.fusiontable':
        return 'Google Fusion Table';

      case 'application/vnd.google-apps.photo':
        return 'Photo';

      case 'application/vnd.google-apps.presentation':
        return 'Google Presentation';

      case 'application/vnd.google-apps.script':
        return 'Google Apps Script';

      case 'application/vnd.google-apps.sites':
        return 'Google Site';

      case 'application/vnd.google-apps.spreadsheet':
        return 'Google Spreadsheet';

      case 'application/vnd.google-apps.unknown':
        return 'Unknown';

      case 'application/vnd.google-apps.video':
        return 'Video';
    }

    // For debugging:
    //return $item['mimeType'];

    return '';
  }

  /**
   * The autocomplete callback function for the linkit plugin.
   *
   * @return
   *   An associative array whose values are an
   *   associative array containing:
   *   - title: A string to use as the search result label.
   *   - description: (optional) A string with additional information about the
   *     result item.
   *   - path: The URL to the item.
   *   - group: (optional) A string with the group name for the result item.
   *     Best practice is to use the plugin name as group name.
   *   - addClass: (optional) A string with classes to add to the result row.
   */
  function autocomplete_callback() {
    $matches = array();

    if ($items = $this->queryGoogle()) {
      foreach ($items as $item) {
        $matches[] = array(
          'title' => $this->buildLabel($item),
          'description' => $this->buildDescription($item),
          'path' => $this->buildPath($item['alternateLink']),
          'group' => $this->buildGroup('Google Drive'),
          'addClass' => $this->buildRowClass('linkit-gauth-drive'),
        );
      }
    }

    return $matches;
  }
}
