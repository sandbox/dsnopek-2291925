<?php
/**
 * @file
 * Linkit Google Drive plugin.
 */

$plugin = array(
  'ui_title' => t('Google Drive'),
  'ui_description' => t('Extend Linkit with Google Drive support.'),
  'handler' => array(
    'class' => 'LinkitGAuthDrivePlugin',
    'file' => 'linkit_gauth_drive.class.php',
  ),
);
